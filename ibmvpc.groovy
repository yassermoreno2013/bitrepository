import groovy.transform.Field

@Field final K_VPC_API_ENDPOINT   = "https://us-south.iaas.cloud.ibm.com"
@Field final K_VPC_TOKEN_ENDPOINT = "https://iam.cloud.ibm.com/identity/token"
@Field final K_VPC_API_VERSION    = "2020-11-17"
@Field final K_VPC_API_KEY        = "0i_6cNaEn98-5l1ukjSkY1chcCk7nWXbJOeRrkW9IoL_"

@Field def iam_token

def initAccessToken () {
    def httpRequestBody = "grant_type=urn:ibm:params:oauth:grant-type:apikey&apikey=${K_VPC_API_KEY}"
    def httpResponse = httpRequest acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_FORM', httpMode: 'POST', requestBody: httpRequestBody, url: K_VPC_TOKEN_ENDPOINT

    def httpResponseContent  = readJSON text: httpResponse.content

    echo "Obtención del IAM Token - httpStatus: ${httpResponse.status} - content.access_token ${httpResponseContent.access_token}"

    iam_token = httpResponseContent.access_token    
}

def serverAction (instanceId, action) {
    def httpRequestBody = """{"type": "${action}"}"""
    def httpResponse = httpRequest  customHeaders: [[name: 'Authorization', value: "Bearer ${iam_token}"]], acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: httpRequestBody, url: "${K_VPC_API_ENDPOINT}/v1/instances/${instanceId}/actions?version=${K_VPC_API_VERSION}&generation=2"
    def httpResponseContent  = readJSON text: httpResponse.content
    echo "httpStatus: ${httpResponse.status} - httpResponse.content: ${httpResponse.content}"
}


def getInstanceState (instanceId) {
    def httpResponse = httpRequest  customHeaders: [[name: 'Authorization', value: "Bearer ${iam_token}"]], acceptType: 'APPLICATION_JSON', contentType: 'APPLICATION_JSON', httpMode: 'GET', url: "${K_VPC_API_ENDPOINT}/v1/instances/${instanceId}?version=${K_VPC_API_VERSION}&generation=2"
    print httpResponse
	def httpResponseContent  = readJSON text: httpResponse.content
    echo "httpStatus: ${httpResponse.status} - httpResponse.content.status: ${httpResponseContent.status}"

    return httpResponseContent.status
}

def isInstanceRunning() {
	initAccessToken()
    def instanceState = getInstanceState("0717_f820d5f2-f16e-4cdf-b1c3-e3822b7d0c9f")
    return instanceState.equals("running")
}

return this